/**
 * Created by dmurphy on 27/09/16.
 */

'use strict'

exports.servePublicDirectory = {
  directory: {
    path: 'public',
  },
};
